package com.vsmet.lib;

public class Car {
    private boolean isRunning = false;
    public void start() {
        isRunning = true;
    }
    public boolean getState() {
        return isRunning;
    }
}
