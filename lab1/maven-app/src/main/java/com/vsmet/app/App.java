package com.vsmet.app;

import com.vsmet.lib.Car;

public class App {
    public static void main( String[] args ) {
        Car c = new Car();
        c.start();
        System.out.println(c.getState());
    }
}
